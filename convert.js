const csv = require('csv-parser');
const fs = require('fs');

let json = []

function toGravite (gravite) {
    if (gravite) {
        return gravite.toUpperCase()
    }
}

function toCisu(row) {
    const cisu = {
        principale: {
            code: row.codeCisu,
            label: row.LabelCisu
        }
    }

    if (row.codeCisu1) {
        cisu.secondaire = {
            code: row.codeCisu1,
            label: row.labelCisu1
        }
    }

    return cisu
}

fs.createReadStream('patho by david.csv')
    .pipe(csv({ separator: ';', headers: ['PE', 'PELabel', 'PE1', 'PE1Label', 'PE2', 'PE2Label', 'gravite', 'codeCisu', 'LabelCisu', 'codeCisu1', 'labelCisu1'] }))
    .on('data', (row) => {
        const PE = json.find(element => element.code.trim() === row.PE.trim())
        if (PE) {
            const PE1 = PE.enfants.find(element => element.code.trim() === row.PE1.trim())
            if (PE1) {
                PE1.enfants.push({
                    cisu: toCisu(row),
                    code: row.PE2,
                    label: row.PE2Label,
                    gravite: toGravite(row.gravite)
                })
            } else {
                PE.enfants.push({
                    cisu: toCisu(row),
                    code: row.PE1,
                    label: row.PE1Label,
                    gravite: toGravite(row.gravite),
                    enfants: []
                })
            }

        } else {
            json.push({
                cisu: toCisu(row),
                code: row.PE,
                label: row.PELabel,
                gravite: toGravite(row.gravite),
                enfants: []
            })
        }
    })
    .on('end', () => {
        console.log(JSON.stringify(json))
    });

    